(function ($) {

  Drupal.behaviors.simple_sharethis = {
    attach: function (context, settings) {
      $("#social-linky-sharethis").click(function () {
        if (typeof window.shareThisLoad == 'undefined') {
          $.getScript('http://w.sharethis.com/button/buttons.js')
            .done(function (script, textStatus) {
              var switchTo5x = true;
              stLight.options({
                publisher: "[PUBLISHER_ID_HERE]",
                doNotHash: true,
                doNotCopy: true,
                hashAddressBar: false
              });
              window.shareThisLoad = true;
              setTimeout(function () {
                $(".st_sharethis_large").trigger("click");
              }, 1500);
            })
            .fail(function () {
              console.log("Couldn't load sharethis.");
            });
          console.log('window share load is undefined');
        }
      });

    }


  }

})(jQuery);  
  